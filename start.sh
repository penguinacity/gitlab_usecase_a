#!/bin/bash

# Assumption: gilab/gitaly/current log forwarded to logstash at 172.17.0.3:5000
#

sudo apt-get update 

docker --version 
if [ $? -ne 0 ] 
then 
# Use docker helper to install documentation and names for 
# this are poorly managered and this seems to be depended on.
# 
curl -fsSL https://get.docker.com -o get-docker.sh 
chmod +x 
./get-docker.sh 
sudo ./get-docker.sh 
sudo groupadd docker 
sudo usermod -aG docker ${USER} 
# relogin after groupadd 
su --shell ${USER} 
fi 

docker run hello-world 
if [ $? -ne 0 ] 
then 
echo "Problem with docker installed aborting." 
exit 3
fi

if [ ! -e "~/.docker/config.json" ]
then
echo "respond with your gitlab credentials"
docker login registry.gitlab.com/penguinacity/gitlab_usecase_a
fi

echo "Loading Elastic images to docker cache."
# DL and run image from remote registry 
# serialize dl from registry so as not to overwhelm connection
#
docker pull registry.gitlab.com/penguinacity/gitlab_usecase_a:elasticv1

docker pull registry.gitlab.com/penguinacity/gitlab_usecase_a:logstashv1

docker pull registry.gitlab.com/penguinacity/gitlab_usecase_a:kibanav1

# These are not serialized but at this point images are local
#
xterm -e "docker run registry.gitlab.com/penguinacity/gitlab_usecase_a:elasticv1" &
sleep 2
xterm -e "docker run registry.gitlab.com/penguinacity/gitlab_usecase_a:logstashv1" &
sleep 2m
xterm -e "docker run registry.gitlab.com/penguinacity/gitlab_usecase_a:kibanav1" &

# Do we have curl?
#
curl --version
if [ $? != 0 ]
then
echo "Getting curl."
sudo apt-get install curl
fi

sleep 20
# Good response is elastic version
echo "Talking to Elastic interface, good response is version."
curl -i "172.17.0.2:9200"

echo "elastic 172.17.0.2:9200"
echo "kibana can be reached at http://172.17.0.4:5601"
echo "using your browser."
