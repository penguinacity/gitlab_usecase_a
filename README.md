# gitlab
## A usecase for egopipe package

## What
Create logic pipeline to analyze git operations and repo management in gitlab. Input will come from
/var/log/gitlab/gitaly/current logs. (json)

## Why do this?
To commonize and make this data more useful in Kibana it undergoes a transformation in our pipeline. 
In other words to aggregate or count occurances of data you need precise fast non analyzed fields that 
have some commonality. This will be making use of egopipe so the code will all be go.

## FYI
See egopipe package at [egopipe on github](https://github.com/wshekrota/egopipe)

## How?
Setup a test environment if you are working from a debian derivative os like ubuntu. Scripts will start
containers for your egopipe to analyze gitlab audit data.
There are 2 methods to do this. The end result of both is the same. You will have 
elasticsearch,logstash and kibana running in separate containers. Logstash will listen on 172.17.0.3:5000
so configure your filebeat with that in mind.
These scripts export xterms so it is advisable to 'ssh -X user@host' the system you want to clone to. In 
that way it will manage the X11 forwarding for you. When you run start.sh windows will pop up.

### -Method #1
Builds the images in your local cache using Dockerfiles for each. This will be made easy with the snippet
script provided.

or 

### -Method #2 (preferred)
Runs images from the registry provided by gitlab. The script start.sh in the repo when you clone it down 
will do this for you. To use this method you must first login the remote registry ie you must get an account.
docker login registry.gitlab.com (enter your gitlab creds)

## Setup
- you have a gitlab vm instance hosted on a debian derivative linux preferable ubuntu.
- you install and configure the lightweight shipper filebeat on that host.
- configure it to talk to your test logstash container 172.17.0.3:5000.
- clone down the repo gitlab_usecase_a and use the included 'start.sh' script to run the 
  images from the remote cache.
- the compiled egopipe containing the usecase code is already included in the apps/logstash directory.
- finally it runs the elastic containers in the order elastic,logstash,kibana. When those images were 
  created the necessary files were placed in the docker build process.
- 3 apps will remain xterms on your desktop.

note: if you do not have docker installed the start.sh script will install it for you.

Alternate install #1 will build the images realtime as well as check for necessary tools like
docker. This is slow and not preferred.
snippet script https://gitlab.com/penguinacity/gitlab_usecase_a/-/snippets/2137189

## Beginning the Gitlab analysis... (what the usecase code does)
We will want to do several things related to git operations and repo management. So your new
pipe should reflect this logic. First it has to identify the tasks to audit.

#### target log gitlab/gitaly (seems to have the info we want)

It is required you send the gitaly logs but may forward any others you want and they will be
included in the database. The usecase code only acts on gitaly logs all others passthru.

```

- create repo
- delete repo
- clone repo (both ssh and http)
- push repo (both ssh and http)

```

You may eventually want to do more operations. I'll consider this the starting point and show you how
I solve part of it. First obviously know the task. Then create some data you can view in Kibana
so you can theorize what the code must do. For instance do a clone then find it.

## Clone from Repo
---
### git_ops:"clone"

grpc.method: "(SSH|Post)UploadPack" (ssh vs https)

grpc.code: "OK"

fields of interest:

grpc.request.glProjectPath - tells you the project and repo name for the clone.

---

So lets make up some top level field names and values for them we will use later.

owner:first word of path

repo: "grpc.request.glProjectPath" project/name


## Push to Repo
---
### git_ops:"push"

grpc.method: "(SSH|InfoRef)ReceivePack"

owner: "project" (project is username)

repo: "project/name"

---

## Create Repo or Project
---
### git_ops:"create"

grpc.method: "CreateRepository"

grpc.code: "OK"

created:grpc.request.glProjectPath

---

## Delete Repo
---
### git_ops:"delete"

grpc.method: "RemoveRepository"

grpc.code: "OK"

This one is a bit strange the ref is moved (w/RenameRepository) and delete happens at a later time.

So the question becomes should you track the rename for time purposed or the remove which appears 

to happen in a batch.

---

Pipe source code
![pipe source code](images/gitlabsource.png)
Source is a set of if/else's grouped to short circuit. By putting the most used points at top
less code will be traversed.

Per Git operation
![visualization git ops](images/git_ops_example.png)

Per repo operations
![visualization per repo](images/per_repo.png)
Yes delete is missing but there is a reason. The associative rename and remove functions do not carry 
through the path information. I am looking into a way to maintain this.
